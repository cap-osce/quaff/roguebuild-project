package roguejava;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class RogueController {

    @RequestMapping("/")
    public String index() {
        if (Math.random() > 0.5) {
            return "SpringBoot RogueApp";
        } else {
            return "Clean App";
        }
    }
}
